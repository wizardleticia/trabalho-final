# trabalho-final


## Conteúdo
- [Descrição](#descrição)
- [Tecnologias](#tecnologias)
- [Referências](#referências)
- [Info do Autor](#Info-do-autor)

## Descrição

Projeto da disciplina IMD0190 - TÓPICOS ESPECIAIS EM BUSINESS INTELIGENCE E ANALYTICS 2 com objetivo de aplicar a cultura DevOps e as funcionalidades de CI/CD. 

## Tecnologias

- Docker
- HTML
- Python
- Devops (CI/CD)
- Flask
- Pylint
- Pytest

# Referências

- https://flask.palletsprojects.com/en/2.0.x/quickstart/

- https://docs.docker.com/get-started/

- https://docs.python.org/3/


# Info do Autor
- Linkedin - [@Letícia Campos do Nascimento](https://www.linkedin.com/in/let%C3%ADcianascimento/)

