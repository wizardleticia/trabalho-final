"""fix bugs."""
from flask import Flask, render_template
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
APP = Flask(__name__)
@APP.route("/")
def  index():
    """return template at index."""
    return render_template('index.html')
if __name__ == "__main__":
    APP.run()
